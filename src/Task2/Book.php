<?php

declare(strict_types=1);

namespace App\Task2;

use Exception;

class Book
{
    public function __construct(
        string $title,
        int $price,
        int $pagesNumber
    ) {
        try {
            if ($price <= 0) {
                throw new Exception('ERROR! price must be greater than 0. You seted: ' . $price);
            }
            if ($pagesNumber <= 0 ) {
                throw new Exception('ERROR! number must be greater than 0. You seted: ' . $pagesNumber);
            }
        } catch (Exception $e) {
            echo $e->getMessage();
            exit();
        }

        $this->title = $title;
        $this->price = $price;
        $this->pagesNumber = $pagesNumber;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getPrice(): int
    {
        return $this->price;
    }

    public function getPagesNumber(): int
    {
        return $this->pagesNumber;
    }
}