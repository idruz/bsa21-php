<?php

declare(strict_types=1);

namespace App\Task2;

use Exception;
use Generator;
class BooksGenerator
{
    public function __construct(
        int $minPagesNumber, 
        array $libraryBooks, 
        int $maxPrice, 
        array $storeBooks
    ) {
        try {
            if ($minPagesNumber <= 0) {
                throw new Exception('ERROR! minPagesNumber must be greater than 0. You seted: ' . $minPagesNumber);
            }
            if ($maxPrice <= 0 ) {
                throw new Exception('ERROR! maxPrice must be greater than 0. You seted: ' . $maxPrice);
            }
        } catch (Exception $e) {
            echo $e->getMessage();
            exit();
        }

        $this->libraryBooks = $libraryBooks;
        $this->storeBooks = $storeBooks;
        $this->minPagesNumber = $minPagesNumber;
        $this->maxPrice = $maxPrice;
    }
    
    public function generate(): Generator
    {
        foreach ($this->libraryBooks as $libraryBook) {
            if ($libraryBook->pagesNumber >= $this->minPagesNumber) {
                yield $libraryBook;
            }
        }

        foreach ($this->storeBooks as $storeBook) {
            if ($storeBook->price < $this->maxPrice) {
                yield $storeBook;
            }
        }
    }
}