<?php

declare(strict_types=1);

namespace App\Task1;

use Exception;

class Car
{
    public function __construct(
        int $id,
        string $image,
        string $name,
        int $speed,
        int $pitStopTime,
        float $fuelConsumption,
        float $fuelTankVolume
    ) {
        try {
            if ($speed <= 0) {
                throw new Exception(
                    'ERROR! Car ' . $name . ' speed must be greater than 0. You seted: ' . $speed
                );
            }
            if ($pitStopTime <= 0 ) {
                throw new Exception(
                    'ERROR! Car ' . $name . ' pitStopTime must be greater than 0. You seted: ' . $pitStopTime
                );
            }
            if ($fuelConsumption <= 0 ) {
                throw new Exception(
                    'ERROR! Car ' . $name . ' fuelConsumption must be greater than 0. You seted: ' . $fuelConsumption
                );
            }
            if ($fuelTankVolume <= 0 ) {
                throw new Exception(
                    'ERROR! Car ' . $name . ' fuelTankVolume must be greater than 0. You seted: ' . $fuelTankVolume
                );
            }
        } catch (Exception $e) {
            echo $e->getMessage();
            exit();
        }

        $this->id = $id;
        $this->image = $image;
        $this->name = $name;
        $this->speed = $speed;
        $this->pitStopTime = $pitStopTime;
        $this->fuelConsumption = $fuelConsumption;
        $this->fuelTankVolume = $fuelTankVolume;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getImage(): string
    {
        return $this->image;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getSpeed(): int
    {
        return $this->speed;
    }

    public function getPitStopTime(): int
    {
        return $this->pitStopTime;
    }

    public function getFuelConsumption(): float
    {
        return $this->fuelConsumption;
    }

    public function getFuelTankVolume(): float
    {
        return $this->fuelTankVolume;
    }
}