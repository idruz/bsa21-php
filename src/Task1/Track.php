<?php

declare(strict_types=1);

namespace App\Task1;

use Exception;
use App\Task1\Car;

class Track
{
    public array $cars = [];

    public function __construct(float $lapLength, int $lapsNumber) 
    {
        try {
            if ($lapLength <= 0) {
                throw new Exception('ERROR! lapLength must be greater than 0. You seted: ' . $lapLength);
            }
            if ($lapsNumber <= 0 ) {
                throw new Exception('ERROR! lapsNumber must be greater than 0. You seted: ' . $lapsNumber);
            }
        } catch (Exception $e) {
            echo $e->getMessage();
            exit();
        }

        $this->lapLength = $lapLength;
        $this->lapsNumber = $lapsNumber;
    }

    public function getLapLength(): float
    {
        return $this->lapLength;
    }

    public function getLapsNumber(): int
    {
        return $this->lapsNumber;
    }

    public function add(Car $car): void
    {
        $this->cars[] = $car;
    }

    public function all(): array
    {
        return $this->cars;
    }

    public function run(): Car
    {
        $carModels = $this->all();
        $carTotalTime = [];

        foreach ($carModels as $key => $carModel) {
            $id = $carModel->id;
            $image = $carModel->image;
            $name = $carModel->name;
            $speed = $carModel->speed;
            $pitStopTime = $carModel->pitStopTime;
            $fuelConsumption = $carModel->fuelConsumption;
            $fuelTankVolume = $carModel->fuelTankVolume;
            $summaryDistance = $this->lapsNumber * $this->lapLength;
            $distanceTime = $summaryDistance / $speed * 3600;
            $distanceTime += $pitStopTime * 
                (
                    ceil (
                        (
                            ( $summaryDistance * $fuelConsumption * 0.01) / $fuelTankVolume
                        )
                    )
                );
            $carTotalTime[$key] = round($distanceTime, 2);
        }

        $keyFastestCar = array_search(min($carTotalTime), $carTotalTime);

        return $carModels[$keyFastestCar];
    }
}