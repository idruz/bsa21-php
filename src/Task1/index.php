<?php

require __DIR__ . "/../../vendor/autoload.php";

use App\Task1\Car;
use App\Task1\Track;

$modelBmw = new Car(
    id: 1,
    image: 'https://pbs.twimg.com/profile_images/595409436585361408/aFJGRaO6_400x400.jpg',
    name: 'BMW',
    speed: 250,
    pitStopTime: 10,
    fuelConsumption: 5,
    fuelTankVolume: 15
);

$modelTesla = new Car(
    id: 2,
    image: 'https://i.pinimg.com/originals/e4/15/83/e41583f55444b931f4ba2f0f8bce1970.jpg',
    name: 'Tesla',
    speed: 200,
    pitStopTime: 5,
    fuelConsumption: 5.3,
    fuelTankVolume: 18
);

$modelFord = new Car(
    id: 3,
    image: 'https://fordsalomao.com.br/wp-content/uploads/2019/02/1499441577430-1-1024x542-256x256.jpg',
    name: 'Ford',
    speed: 220,
    pitStopTime: 5,
    fuelConsumption: 6.1,
    fuelTankVolume: 18.5
);

$trackData = new Track(lapLength: 2, lapsNumber: 10);
$trackData->add($modelBmw);
$trackData->add($modelTesla);
$trackData->add($modelFord);

print_r($trackData->run());
