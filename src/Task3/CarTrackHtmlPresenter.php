<?php

declare(strict_types=1);

namespace App\Task3;

use Exception;
use App\Task1\Car;
use App\Task1\Track;

class CarTrackHtmlPresenter
{
    public function present(Track $track): string
    {
        $carModels = $track->all();
        try {
            if (empty($carModels)) {
                throw new Exception(
                    'ERROR! Needed add Cars to track'
                );
            }
        } catch (Exception $e) {
            echo $e->getMessage();
            exit();
        }

        $htmlContent = '<div class="cars-track">';
            $htmlContent .= '<div class="container">';
                $htmlContent .= '<div class="car-cards">';

                foreach ($carModels as $carModel) {
                    $htmlContent .= '<div class="card">';
                        $htmlContent .= '<h2>' . $carModel->name . '</h2>';
                        $htmlContent .= '<img src="' . $carModel->image . '">';	
                        $htmlContent .= '<div class="car-info">';
                        $htmlContent .= '<div class="item">&#x1F40C; Speed: ' . $carModel->speed . ' km/h</div>';
                        $htmlContent .= '<div class="item">&#x23F1; Pit Stop Time: ' . $carModel->pitStopTime . ' s.</div>';
                        $htmlContent .= '<div class="item">&#x26FD; Fuel Consumption: ' . $carModel->fuelConsumption . ' l.</div>';
                        $htmlContent .= '<div class="item">&#x1F6E2; Fuel Tank Volume: ' . $carModel->fuelTankVolume . ' l.</div>';
                        $htmlContent .= '</div>';
                    $htmlContent .= '</div><!-- end .card -->';
                }

                $htmlContent .= '</div><!-- end .car-cards -->';
            $htmlContent .= '</div><!-- end .container -->';
        $htmlContent .= '</div><!-- end .cars-track -->';

        return $htmlContent;
    }
}